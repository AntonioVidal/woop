package com.example.myapplication.api

interface ConnectionListener<T> {

    fun success(data: T)
    fun fail(errorMessage: String)

}