package com.example.myapplication.api

import com.example.myapplication.model.Event
import com.example.myapplication.model.response.UserRequest
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface EventContract {

    @GET("/api/events")
    fun getEvents(
    ): Call<MutableList<Event>>

    @POST("api/checkin")
    fun checkIn(
        @Body checkInName: UserRequest
    ): Call<JsonObject>

}