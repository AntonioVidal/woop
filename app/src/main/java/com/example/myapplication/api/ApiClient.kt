package com.example.myapplication.api

import com.example.myapplication.model.Event
import com.example.myapplication.model.response.UserRequest
import com.google.gson.Gson
import com.google.gson.JsonObject

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    private val API_URL_BASE = "http://5b840ba5db24a100142dcd8c.mockapi.io"

    private val requestClient by lazy {
        client.create(EventContract::class.java)
    }

    private val client: Retrofit = Retrofit.Builder().let {
        it.baseUrl(API_URL_BASE)
        it.addConverterFactory(GsonConverterFactory.create())
        it.build()
    }

    fun getEvents(connectionListener: ConnectionListener<MutableList<Event>>) {
        requestClient.getEvents().enqueue(object : Callback<MutableList<Event>> {
            override fun onFailure(call: Call<MutableList<Event>>, t: Throwable) {
                connectionListener.fail("Erro de conexão")
            }

            override fun onResponse(call: Call<MutableList<Event>>, response: Response<MutableList<Event>>) {
                if (response.isSuccessful) {
                    connectionListener.success(response.body()!!)
                } else {
                    connectionListener.fail(response.message())
                }
            }
        })
    }

    fun checkIn(userRequest: UserRequest, connectionListener: ConnectionListener<JsonObject>) {
        requestClient.checkIn(userRequest).enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                connectionListener.fail("Erro de conexão")
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {
                    connectionListener.success(response.body()!!)
                } else {
                    connectionListener.fail(response.message())
                }
            }
        })
    }

}