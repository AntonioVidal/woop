package com.example.myapplication.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.api.ApiClient
import com.example.myapplication.api.ConnectionListener
import com.example.myapplication.model.Event
import com.example.myapplication.model.response.UserRequest
import com.google.gson.JsonObject

class EventsViewModel : ViewModel() {

    val eventsLiveData = MutableLiveData<MutableList<Event>>()
    val checkInResponse: MutableLiveData<JsonObject> = MutableLiveData()
    val error = MutableLiveData<String>()

    fun getEvents() {
        ApiClient.getEvents(object: ConnectionListener<MutableList<Event>> {
            override fun success(data: MutableList<Event>) {
                eventsLiveData.value = data
            }

            override fun fail(errorMessage: String) {
                error.value = errorMessage
            }
        })
    }

    fun checkIn(id: String, name: String, email: String) {
        ApiClient.checkIn(UserRequest(id, name, email), object: ConnectionListener<JsonObject> {
            override fun success(data: JsonObject) {
                checkInResponse.value = data
            }

            override fun fail(errorMessage: String) {
                error.value = errorMessage
            }

        })
    }


}