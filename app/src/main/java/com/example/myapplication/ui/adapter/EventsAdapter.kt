package com.example.myapplication.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.myapplication.R
import com.example.myapplication.model.Event
import com.example.myapplication.util.FormatterUtil
import kotlinx.android.synthetic.main.event_list_item.view.*

class EventsAdapter(
    private val context: Context,
    private val events: MutableList<Event>,
    val onItemClickListner: ((event: Event) -> Unit)) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    val option: RequestOptions by lazy {
        RequestOptions().centerCrop().placeholder(R.drawable.design_password_eye).error(R.drawable.abc_btn_check_material)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return EventViewHolder(LayoutInflater.from(context).inflate(R.layout.event_list_item, parent, false), onItemClickListner)
    }

    override fun getItemCount(): Int {
        return events.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as EventViewHolder

        events[position].run {
            holder.let {
                it.eventTitle.text = title
                it.eventDate.text = FormatterUtil.formatTime(date!!)
                it.eventPrice.text = FormatterUtil.formatCurrency(price)
                Glide.with(context).load(image).apply(option).into(it.eventPicture)
                it.bindView(this)
            }
        }
    }

    inner class EventViewHolder(itemView: View, val onItemClickListner: (event: Event) -> Unit) : RecyclerView.ViewHolder(itemView) {
        var eventPicture: ImageView = itemView.event_imageView
        var eventPrice: TextView = itemView.event_price_text
        var eventTitle: TextView = itemView.event_title_text
        var eventDate: TextView = itemView.event_date_text

        fun bindView(event: Event) {
            itemView.setOnClickListener {
                onItemClickListner.invoke(event)
            }
        }
    }
}