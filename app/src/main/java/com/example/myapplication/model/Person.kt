package com.example.myapplication.model

import java.io.Serializable

data class Person(
    var id: String,
    var eventId: String,
    var name: String,
    var picture: String
) : Serializable