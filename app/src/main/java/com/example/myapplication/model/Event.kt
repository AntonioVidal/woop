package com.example.myapplication.model

import java.io.Serializable

class Event(
    var id: String?,
    var title: String?,
    var price: Double?,
    var latitude: String?,
    var longitude: String?,
    var image: String?,
    var description: String?,
    var date: Long?,
    var people: MutableList<Person>?,
    var cupons: MutableList<Coupon>?
) : Serializable
