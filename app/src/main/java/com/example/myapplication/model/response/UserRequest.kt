package com.example.myapplication.model.response

class UserRequest (
    var eventId: String,
    var name: String,
    var email: String
)