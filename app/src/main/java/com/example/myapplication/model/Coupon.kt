package com.example.myapplication.model

import java.io.Serializable

data class Coupon (
        var id: String,
        var eventId: String,
        var discount: Long
) : Serializable