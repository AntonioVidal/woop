package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.ui.EventsViewModel
import kotlinx.android.synthetic.main.activity_check_in.*
import kotlinx.android.synthetic.main.activity_check_in.event_toolbar
import kotlinx.android.synthetic.main.activity_events.*

class CheckInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in)

        setSupportActionBar(event_toolbar)

        supportActionBar?.run {
            title = getString(R.string.events_title)
        }

        val viewModel = ViewModelProviders.of(this).get(EventsViewModel::class.java)
        viewModel.eventsLiveData.observe(this, Observer {
            it?.let {
                Toast.makeText(this, R.string.check_in_success, Toast.LENGTH_LONG).show()
                finishActivity()
            }
        })

        checkin_button.setOnClickListener {
            viewModel.checkIn(
                intent.getStringExtra("EXTRA_MESSAGE")!!,
                checkin_dialog_name.text.toString(),
                checkin_dialog_email.text.toString()
            )
        }

        viewModel.error.observe( this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    fun finishActivity() {
        finish()
    }
}
