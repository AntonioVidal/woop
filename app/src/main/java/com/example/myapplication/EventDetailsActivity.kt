package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.myapplication.model.Event
import kotlinx.android.synthetic.main.activity_event_details.*

class EventDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)

        setSupportActionBar(event_detail_toolbar)

        supportActionBar?.run {
            title = getString(R.string.events_title)
        }

        val option: RequestOptions by lazy {
            RequestOptions().centerCrop().placeholder(R.drawable.design_password_eye).error(R.drawable.abc_btn_check_material)
        }

        val title = intent.getStringExtra(EXTRA_EVENT_TITLE)
        val description = intent.getStringExtra(EXTRA_EVENT_DESCRIPTION)
        val price = intent.getStringExtra(EXTRA_EVENT_PRICE)
        val image = intent.getSerializableExtra(EXTRA_EVENT_IMAGE)
        Glide.with(this).load(image).apply(option).into(event_detail_image)
        var eventId = intent.getStringExtra("eventId")

        event_detail_button.setOnClickListener {
            startActivity(Intent(this, CheckInActivity::class.java)
                .putExtra("EXTRA_MESSAGE", eventId)
            )
        }

        event_detail_title_text.text = title
        event_detail_description_text.text = description
        event_detail_price_text.text =  price
    }

    companion object {
        private const val EXTRA_EVENT_TITLE = "EXTRA_EVENT_TITLE"
        private const val EXTRA_EVENT_DESCRIPTION = "EXTRA_EVENT_DESCRIPTION"
        private const val EXTRA_EVENT_IMAGE = "EXTRA_EVENT_IMAGE"
        private const val EXTRA_EVENT_PRICE = "EXTRA_EVENT_PRICE"

        fun getStartIntent(context: Context, event: Event) : Intent {
            return Intent(context, EventDetailsActivity::class.java).apply {
                putExtra(EXTRA_EVENT_TITLE, event.title)
                putExtra(EXTRA_EVENT_DESCRIPTION, event.description)
                putExtra(EXTRA_EVENT_IMAGE, event.image)
                putExtra(EXTRA_EVENT_PRICE, event.price)
            }
        }
    }

}
