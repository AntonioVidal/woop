package com.example.myapplication

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.api.ApiClient
import com.example.myapplication.api.ConnectionListener
import com.example.myapplication.model.Event
import com.example.myapplication.ui.EventsViewModel
import com.example.myapplication.ui.adapter.EventsAdapter
import kotlinx.android.synthetic.main.activity_events.*

class EventsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)

        setSupportActionBar(event_toolbar)

        supportActionBar?.run {
            title = getString(R.string.events_title)
        }

        val viewModel = ViewModelProviders.of(this).get(EventsViewModel::class.java)
        viewModel.eventsLiveData.observe(this, Observer {
            it?.let { event ->
                setAdapter(event)
            }
        })

        viewModel.getEvents()
    }

    fun setAdapter(events: MutableList<Event>) {
        val context = this@EventsActivity

        with(event_recycler) {
            adapter = EventsAdapter(context, events) {event ->
                val intent = EventDetailsActivity.getStartIntent(context, event)
                    .putExtra("eventId", event.id)
                context.startActivity(intent)
            }
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }

        event_progressbar.visibility = View.GONE
    }
}
