package com.example.myapplication.util

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import org.joda.time.DateTime
import java.util.*

class FormatterUtil {

    companion object {
        fun formatCurrency(value: Double?): String = String.format(Locale("pt", "BR"), "R$ %,.2f", value)

        fun getUpdatedAt(): String = "Atualizado em: ${DateTime.now().toString("dd MMM yyyy - hh:mm:ss")}"

        fun formatTime(value: Long): String = DateTime(value).toString("dd/MM/yyyy - HH:mm:ss")

        fun getBrandDrawable(context: Context, carrier: String): Drawable? {
            carrier.replace("cartao ", "").trim { it <= ' ' }.replace(" ", "_").toLowerCase().run {
                val resourceId = context.resources.getIdentifier("ic_$this", "drawable", context.packageName)
                if (resourceId != 0) {
                    return ContextCompat.getDrawable(context, resourceId)
                }
            }

            return null
        }

        fun getDefaultLocale(): Locale {
            return Locale("pt", "BR")
        }
    }
}
