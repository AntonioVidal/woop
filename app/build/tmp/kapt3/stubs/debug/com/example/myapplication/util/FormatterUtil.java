package com.example.myapplication.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/example/myapplication/util/FormatterUtil;", "", "()V", "Companion", "app_debug"})
public final class FormatterUtil {
    public static final com.example.myapplication.util.FormatterUtil.Companion Companion = null;
    
    public FormatterUtil() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\tJ\u0018\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0004J\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\u0004\u00a8\u0006\u0012"}, d2 = {"Lcom/example/myapplication/util/FormatterUtil$Companion;", "", "()V", "formatCurrency", "", "value", "", "(Ljava/lang/Double;)Ljava/lang/String;", "formatTime", "", "getBrandDrawable", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "carrier", "getDefaultLocale", "Ljava/util/Locale;", "getUpdatedAt", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String formatCurrency(@org.jetbrains.annotations.Nullable()
        java.lang.Double value) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUpdatedAt() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String formatTime(long value) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final android.graphics.drawable.Drawable getBrandDrawable(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String carrier) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Locale getDefaultLocale() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}