package com.example.myapplication.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0014\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0003H\'\u00a8\u0006\n"}, d2 = {"Lcom/example/myapplication/api/EventContract;", "", "checkIn", "Lretrofit2/Call;", "Lcom/google/gson/JsonObject;", "checkInName", "Lcom/example/myapplication/model/response/UserRequest;", "getEvents", "", "Lcom/example/myapplication/model/Event;", "app_debug"})
public abstract interface EventContract {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "/api/events")
    public abstract retrofit2.Call<java.util.List<com.example.myapplication.model.Event>> getEvents();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "api/checkin")
    public abstract retrofit2.Call<com.google.gson.JsonObject> checkIn(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.example.myapplication.model.response.UserRequest checkInName);
}