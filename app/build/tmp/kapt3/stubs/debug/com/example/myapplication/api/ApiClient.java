package com.example.myapplication.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013J\u001a\u0010\u0015\u001a\u00020\u000f2\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u00160\u0013R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0018"}, d2 = {"Lcom/example/myapplication/api/ApiClient;", "", "()V", "API_URL_BASE", "", "client", "Lretrofit2/Retrofit;", "requestClient", "Lcom/example/myapplication/api/EventContract;", "kotlin.jvm.PlatformType", "getRequestClient", "()Lcom/example/myapplication/api/EventContract;", "requestClient$delegate", "Lkotlin/Lazy;", "checkIn", "", "userRequest", "Lcom/example/myapplication/model/response/UserRequest;", "connectionListener", "Lcom/example/myapplication/api/ConnectionListener;", "Lcom/google/gson/JsonObject;", "getEvents", "", "Lcom/example/myapplication/model/Event;", "app_debug"})
public final class ApiClient {
    private static final java.lang.String API_URL_BASE = "http://5b840ba5db24a100142dcd8c.mockapi.io";
    private static final kotlin.Lazy requestClient$delegate = null;
    private static final retrofit2.Retrofit client = null;
    public static final com.example.myapplication.api.ApiClient INSTANCE = null;
    
    private final com.example.myapplication.api.EventContract getRequestClient() {
        return null;
    }
    
    public final void getEvents(@org.jetbrains.annotations.NotNull()
    com.example.myapplication.api.ConnectionListener<java.util.List<com.example.myapplication.model.Event>> connectionListener) {
    }
    
    public final void checkIn(@org.jetbrains.annotations.NotNull()
    com.example.myapplication.model.response.UserRequest userRequest, @org.jetbrains.annotations.NotNull()
    com.example.myapplication.api.ConnectionListener<com.google.gson.JsonObject> connectionListener) {
    }
    
    private ApiClient() {
        super();
    }
}